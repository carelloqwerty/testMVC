<?php

/**
 * Description of conect
 *
 * @author Serg
 */

namespace Src\DbConection;

use PDO;

class Db
{    private static $db=false;

    public static function getConection()
    {
        if (!self::$db){
            require ROOT . '/src/DbConection/dbParams.php';
            self::$db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $password, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                //PDO::ATTR_EMULATE_PREPARES => FALSE,
            ]);
            
            return self::$db;
            
        } else {
            return self::$db;}
    }
}
