<?php
/**
 * Created by PhpStorm.
 * User: phpuser
 * Date: 19.07.18
 * Time: 11:52
 */

namespace App\Controller;

use App\Models\Product;

class CategoryController
{

    public function actionCategoryAll($param)
    {
        $productList = [];
        $category = Product::categories();
//        $productList = Product::getItemByCategory($param);
        if ($param) {
            $productList = Product::getItemByCategory($param);
        }

        $categoryName = ucfirst(substr($_SERVER['REQUEST_URI'], 1));
        require_once ROOT . '/app/Views/main.php';

        return true;
    }
}