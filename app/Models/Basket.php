<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;
use App\Models\Product;
use Src\DbConection\Db;
/**
 * Description of Basket
 *
 * @author Serg
 */
class Basket {

    /**
     * Добавление товара в корзину (сессию)
     * @param int $id
     */
    public static function addProduct($id)
    {
        $id = intval($id);

        // Пустой массив для товаров в корзине
        $productsAdded = [];
        // Если в корзине уже есть товары (они хранятся в сессии)
        if (isset($_SESSION['products'])) {
            // То заполним наш массив товарами
            $productsAdded = $_SESSION['products'];
        }

        // Если товар есть в корзине, но был добавлен еще раз, увеличим количество
        if (array_key_exists($id, $productsAdded)) {
            $productsAdded[$id] ++;
        } else {
            // Добавляем нового товара в корзину
            $productsAdded[$id] = 1;
        }

        $_SESSION['products'] = $productsAdded;

        return self::countItems();
    }
    
    public static function removeProduct($id)
    {
        $id = intval($id);
        $productsAdded = [];

        if (isset($_SESSION['products'])) {
            $productsAdded = $_SESSION['products'];
        }

        if (array_key_exists($id, $productsAdded)) {
            $productsAdded[$id] --;
            if($productsAdded[$id]==0){
                unset($productsAdded[$id]);
            }
        }

        $_SESSION['products'] = $productsAdded;
        if($_SESSION['products']==[]){
            unset($_SESSION['products']);
            
        }
        
        return self::countItems();
    }
    public static function removeProductId($id){
        $id = intval($id);
        $productsAdded = [];

        if (isset($_SESSION['products'])) {
            $productsAdded = $_SESSION['products'];
        }

        if (array_key_exists($id, $productsAdded)) {
            unset($productsAdded[$id]);
        }

        $_SESSION['products'] = $productsAdded;
        if($_SESSION['products']==[]){
            unset($_SESSION['products']);
        }
    }

    /**
     * Подсчет количество товаров в корзине (в сессии)
     * @return int 
     */
    public static function countItems()
    {
        if (isset($_SESSION['products'])) {
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 'пусто';
        }
    }

    public static function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        } else {
        return false;
        }
    }

    public static function getTotalPrice($products)
    {
        $productsInCart = self::getProducts();

        $total = 0;
        
        if ($productsInCart) {            
            foreach ($products as $item) {
                $total += $item['price'] * $productsInCart[$item['id']];
            }
        }

        return $total;
    }
    
    public static function listBasket(){


        $productsInBasket = false;

        $productsInBasket = Basket::getProducts();

        if ($productsInBasket) {
            $id = array_keys($productsInBasket);
            $products = Product::getItemById($id);

            $totalPrice = Basket::getTotalPrice($products);
        } else {
            echo 'В корзине ничего нет.';
        }
    }

    public static function addOrder(){
        if (isset($_SESSION['user'])){
        $user=$_SESSION['user'];
        $db = Db::getConection();

        $sql="INSERT INTO `student_db`.`order` (`user_id`) VALUES ('$user')";
        $result = $db->query($sql);

        $sql="SELECT `id` FROM student_db.`order` where user_id=$user order by `id` desc limit 1";
        $result = $db->query($sql);
        $orderId=false;
        while ($row =$result->fetch()) {
            $orderId = $row['id'];
        };
        
        foreach ($_SESSION['price'] as $key=>$value) {
            $sql = "INSERT INTO `student_db`.`goods_order` (`goods_id`, `order_id`, `price`, `amount`) VALUES ('$key', '$orderId', '$value[0]', '$value[1]')";
            $result = $db->query($sql);
        }
        unset($_SESSION['products']);
            header("Location: /basket");
        } else {
            header("Location: /login");
        }
        
    }
}
//put your code here
