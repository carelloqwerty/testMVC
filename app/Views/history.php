<?php
require_once "header.php";
?>
<div class="bs-example" data-example-id="simple-table">
    <?php if (isset($history)){?>
        <h1>История заказов</h1>
            <?php foreach ($history as $key=>$order){$totalPrice=0;?>
                <table class="table">
                    <caption>Заказ №<?=$key;?></caption>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Наименование товара</th>
                        <th>количество</th>
                        <th>цена за единицу</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php  foreach ($order as $values){ $i=1; $totalPrice+=$values['price']*$values['amount'] ?>
                            <tr>
                                <th scope="row"><?=$i?></th>
                                <td><?=$values['brend'].' '.$values['name']?></td>
                                <td><?=$values['amount']?></td>
                                <td><?=$values['price']/100?> грн</td>
                                </tr>
                        <?php $i++;}?>
                            <tr>
                                <td></td>
                                <td>Всего</td>
                                <td></td>
                                <td><?=$totalPrice/100?> грн</td>
                            </tr>
                    </tbody><?php }?>
                </table>
            <?php } else {?>
        <h1>Вы еще ничего не покупали</h1> 
    <?php }?>
</div>
</body>
<?php
require_once "footer.php";
?>