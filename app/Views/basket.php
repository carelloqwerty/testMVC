<?php
require_once "header.php";
?>
<div class="bs-example" data-example-id="simple-table">
    <table class="table">
        <?php if (isset($products)){$i=1;?>
            <caption>Корзина</caption>
            <thead>
            <tr>
                <th>#</th>
                <th>Наименование товара</th>
                <th>количество</th>
                <th>цена за единицу</th>
            </tr>
            </thead>
            <tbody>
                <?php  foreach ($products as $values){?>
                    <tr>
                        <th scope="row"><?=$i?></th>
                        <td><?=$values['brend'].' '.$values['name']?></td>
                        <td><?=$basket[$values['id']]?></td>
                        <td><?=$values['price']/100?> грн</td>
                        <td>
                            <a type="button" class="btn btn-success " data-dismiss="modal" href="/basket/add/<?=$values['id']?>">Add</a>
                            <a type="button" class="btn btn-success " data-dismiss="modal" href="/basket/remove/<?=$values['id']?>">Remove</a>
                        </td>
                        <td><a type="button" class="btn btn-success " data-dismiss="modal" href="/basket/removeId/<?=$values['id']?>">Remove position</a></td>
                    </tr>
                <?php $i++;}?>
                    <tr>
                        <td></td>
                        <td>Всего</td>
                        <td></td>
                        <td><?=$totalPrice/100?> грн</td>
                        <td><a class="btn btn-primary" role="button" href="/basket/proove">Подтвердить</a></td>
                    </tr>
            </tbody>
        <?php } else {?>
            <h1>Нет товаров в корзине</h1>
        <?php }?>
    </table>
</div>
</body>
<?php
require_once "footer.php";
?>